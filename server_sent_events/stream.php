<?php
/**
 * I think is a good idea to write your own event stream server (Apache is not that flexible)
 */

// set content type for browser
header("Content-Type: text/event-stream");

$sampleEvents = array(
	array( 'data' => '[192.168.0.109] Revision 23 deployed to Staging environment', 'event' => 'info'),
	array( 'data' => '[192.168.0.87] Warning server CPU load (7.11, 7.4, 7.81 )', 'event' => 'warning'),
	array( 'data' => '[192.168.0.101] Status Check Failed (Instance)', 'event' => 'critical'),
	array( 'data' => '[192.168.0.109] Failed to deploy revision 11 to Production environment', 'event' => 'critical')
);

// main loop
while(!empty($sampleEvents)){

	$entry = array_pop($sampleEvents);
	
	// send event type and event data
	printf("event: %s\n", $entry['event']);
	printf("data: %s\n\n", $entry['data']);
	
	// flush please
	ob_flush();
	flush();
	
	sleep(1);
}
