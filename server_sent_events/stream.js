var http = require('http');

var sampleEvents = [
	{'data': '[192.168.0.109] Revision 23 deployed to Staging environment', 'event': 'info'},
	{'data': '[192.168.0.87] Warning server CPU load (7.11, 7.4, 7.81 )', 'event': 'warning'},
	{'data': '[192.168.0.101] Status Check Failed (Instance)', 'event': 'critical'},
	{'data': '[192.168.0.109] Failed to deploy revision 11 to Production environment', 'event': 'critical'}
];

var server =  http.createServer(function (request, response) {
	response.writeHead(200, {"Content-Type": "text/event-stream"});
	
	
	var timer = setInterval(function () {
	
		var item = sampleEvents.pop();
		
		console.log(item);
		
		response.write("event: " + item.event + "\n");
		response.write("data: " + item.data + "\n\n");
		
		if(sampleEvents.length == 0){
			clearTimeout(timer);
			response.end();
		}
		
	}, 1000);
});

server.listen(8080);