# HTML 5 Canvas

## Canvas2D (introduction)

### Canvas Element
https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Canvas_tutorial/Using_images
<canvas> is an HTML element which can be used to draw graphics using JavaScript.
To access the 2d rendering context call HTMLCanvasElement.getContext('2d')

### Drawing Rectangles

Canvas supports only one primitive shape: rectangles. 
All other shapes must be created by combining one or more paths.

fillRect(x, y, width, height)
strokeRect(x, y, width, height)
clearRect(x, y, width, height)


### Drawing paths

beginPath()
moveTo(x, y)
lineTo(x, y)
arc(x, y, radius, startAngle, endAngle, anticlockwise)
quadraticCurveTo(cp1x, cp1y, x, y)
bezierCurveTo(cp1x, cp1y, cp2x, cp2y, x, y)
rect(x, y, width, height)
closePath()
stroke()
fill()

### Images
The canvas API is able to use:

* HTMLImageElement
* HTMLVideoElement
* HTMLCanvasElement

drawImage(image, x, y)
drawImage(image, x, y, width, height)
drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight)

### Colors

fillStyle = color
strokeStyle = color
globalAlpha = transparencyValue (0.0 - 1.0)

### Line styles
lineWidth = value // thickness
lineCap = type // Sets the appearance of the ends of lines. ()
* butt // The ends of lines are squared off at the endpoints.
* round // The ends of lines are rounded.
* square // The ends of lines are squared off by adding a box with an equal width and half the height of the line's thickness.
lineJoin = type // corners
* round // Rounds off the corners
* bevel // Fills an additional triangular area
* miter // Connected segments are joined by extending their outside edges
miterLimit = value

### Gradients
createLinearGradient(x1, y1, x2, y2)
createRadialGradient(x1, y1, r1, x2, y2, r2)
gradient.addColorStop(position, color) // position (0.0 - 1.0)

### Patterns
createPattern(image, type) // repeat, repeat-x, repeat-y, no-repeat

### Shadows
shadowOffsetX = float
shadowOffsetY = float
shadowBlur = float
shadowColor = <color>

### Transformations
save()
restore()

translate(x, y)
rotate(angle)
scale(x, y)

### Compositing

@TODO

### Basic animations

@TODO