# Html 5 APIs

## Web Workers
Web Workers provide a way web content to run scripts in background threads.
Once created a worker can communicate with the parent thru messages.
Code executing inside a worker has no access to global context or DOM.

Workers can perform I/O using XMLHttpRequest

### Thread safety
* DOM
* UI
* Messages

### Worker 

Worker(aURL) constructor takes one argument, a url to a javscript file
This script must obey the same-origin policy.

### Events
* message

Methods
worker.postMessage(aMessage, transferList);
worker.terminate();

### Browser Support
http://caniuse.com/#feat=webworkers


## Web Messaging
Web messaging is a way for documents in separate browsing contexts to share
data without the DOM being exposed to malicious cross-origin scripting. 
Unlike other forms of cross-site communication (cross-domain XMLHttpRequest,
or dynamic script insertion), web messaging never directly exposes the DOM.

### Cross Document Messaging
Allows documents with diferite origins to comunicate.

### postMessage
otherWindow.postMessage(message, targetOrigin, [transfer]);

### MessageChannel 
Channel messaging provides a means of direct, two-way communication between browsing contexts.
Each message chanel has 2 ports.

### MessagePort 
postMessage() // Posts a message through the channel.
start() // Begins the dispatch of messages received on the port.
close() // Closes and deactivates the port.

### Browser Support
http://caniuse.com/#feat=channel-messaging

## Server sent events
Server-sent events is a technology for where a browser gets automatic 
updates from a server via HTTP connection.

### EventSource 
The EventSource interface is used to receive server-sent events. It connects to a server over HTTP and receives events in text/event-stream format without closing the connection.
Events: error, message, open, custom_event

### Protocol
event: {eventname} \n // default is message
data: {any text data} \n
id: {id} (optional ) \n // used for last event id
\n \\ end of message

### Browser support
http://caniuse.com/#feat=eventsource

## Web Sockets
The WebSocket API provides realtime communications in browser.

### WebSocket 
The WebSocket object has 2 constructors:
WebSocket(url, protocol );
WebSocket(url, protocols[] );

### Attributes 
* binaryType // Blob | ArrayBuffer
* protocol
* readyState
* url

### Events
* close
* error
* message
* open

### Ready state
CONNECTING
OPEN
CLOSING
CLOSED

### Methods
close(code, reason ) // @see docs for full list of codes
send(string | ArrayBuffer | Blob )

### Protocol
Every WebSocket connection begins with an HTTP request with the header Upgrade

### Opening Handshake

// request
GET /echo HTTP/1.1
Host: localhost
Origin: http://localhost
Sec-WebSocket-Key: 7+C600xYybOv2zmJ69RQsw==
Sec-WebSocket-Version: 13
Upgrade: websocket

// reponse
101 Switching Protocols
Connection: Upgrade
Date: Wed, 10 Ian 2014 03:39:49 GMT
Sec-WebSocket-Accept: fYoqiH14DgI+5ylEMwM2sOLzOi0=
Upgrade: WebSocket


### Key Response
var KEY_SUFFIX = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
var hashWebSocketKey = function(key) {
var sha1 = crypto.createHash("sha1");
sha1.update(key + KEY_SUFFIX, "ascii");
	return sha1.digest("base64");
}

### Frames
 * Opcodes - describes the type of message (Text, Binary, Close, Ping, Pong)
 * Length
 * Masking
 
### Browser Support
http://caniuse.com/#feat=websockets


## Drag And Drop
In HTML5, drag and drop is part of the standard, and any element can be draggable.

### Events
dragstart // Fired on an element when a drag is started
dragenter // Fired when the mouse is first moved over an element while a drag is occurring.
dragover // This event is fired as the mouse is moved over an element when a drag is occuring.
dragleave // This event is fired when the mouse leaves an element while a drag is occurring. 
drag // This event is fired at the source of the drag, that is, the element where dragstart was fired, during the drag operation.
drop // The drop event is fired on the element where the drop occurred at the end of the drag operation. 
dragend // The source of the drag will receive a dragend event when the drag operation is complete, whether it was successful or not.

### dataTransfer
All drag events have a property called dataTransfer which is used to hold the drag data.

### Browser Support
http://caniuse.com/#feat=dragndrop

## Video Element
Inside video tag multiple source tags can be added for browser comparibility.

<video src="video.webm" controls poster="image.jpg" id="video"></video>

### Browsers and video formats

* Theora (video/ogg)
* H.264  (H.264/MPEG-4)
* WebM [VP8 / VP9] (video/webm)

http://en.wikipedia.org/wiki/HTML5_video#Browser_support

### Attributes
* autoplay
* preload
* poster
* controls
* height & width
* loop
* muted

### Properties
* currentTime
* volume
* muted
* playbackRate
* currentSrc
* videoWidth && videoHeight

### Methods
canPlayType('video/ogg') => (probably, maybe, '') 
load()
pause()
play()


### Browser Support
http://caniuse.com/#feat=video

## Considerations

## End