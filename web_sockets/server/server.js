var WebSocketServer = require('ws').Server;

// implement broadcast method
WebSocketServer.prototype.broadcast = function (data){
	for(index in this.clients){
		this.clients[index].send(data);
	}
};

// create new server instance
var webSocketServer = new WebSocketServer({port: 8080});

// listen for connection
webSocketServer.on('connection', function (webSocket) {
	
	console.log('connection: %s', webSocket);
	// on message brodcast message to all clients
	webSocket.on('message', function(message) {
		console.log('message: %s', message);
		webSocketServer.broadcast(message);
	});
	
	// send welcome message
	// webSocket.send('Welcome to the server !');
});

